import java.util.Random;
import java.util.Scanner;

public class Main {
	public static boolean end = false;

	public static int publicKey;
	public static int modulo;

	public static int privateKey;

	public static long privatePublicKeyA;
	
	public static long privatePublicKeyB;
	
	public static long sharedSecret;
	
	public static Random rand = new Random();
	
	public static void main(String[] args) {
		if (args.length < 2) {
			System.err.println("Please enter a public key and a modulo");
			return;
		}

		
		try {
			publicKey = Integer.parseInt(args[0]);
		} catch (NumberFormatException e) {
			System.err.println("Public key isn't a valid number");
			return;
		}
		
		try {
			modulo = Integer.parseInt(args[1]);
		} catch (NumberFormatException e) {
			System.err.println("Modulo isn't a valid number");
			return;
		}
		
		System.out.print("Please enter private key: ");
		Scanner scanner = new Scanner(System.in);
		privateKey = scanner.nextInt();

		for (int i = 0; i < privateKey; i++) {
			privatePublicKeyA *= publicKey;
			if (privatePublicKeyA == 0) privatePublicKeyA = publicKey;
			
			privatePublicKeyA %= modulo;
		}
		
		System.out.println();
		System.out.println("Your Private-Public key is: " + privatePublicKeyA);
		
		System.out.print("Please enter Private-Public key of other person to connect to: ");
		privatePublicKeyB = scanner.nextInt();
		
		System.out.println();

		for (int i = 0; i < privateKey; i++) {
			sharedSecret *= privatePublicKeyB;
			if (sharedSecret == 0) sharedSecret = privatePublicKeyB;
			
			sharedSecret %= modulo;
		}

		while (true) {
			System.out.print("Enter message to decrypt or incrypt: ");
			
			String message;
			while ((message = scanner.nextLine()).isEmpty());
			String result = "";
			
			for (int i = 0; i < message.length(); i++) {
				char ch = message.charAt(i);
				result += (char) (ch ^ sharedSecret);
				rand.setSeed(sharedSecret);
				sharedSecret = rand.nextLong();
			}
			
			System.out.println(result);
		}
	}
}
